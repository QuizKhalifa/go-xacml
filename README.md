# go-xacml

An implementation of XACML PDP/PAP using golang.

The policy struct implementations are from [this github repo](https://github.com/Hasdcorona/go-xacml) (vendor/policy). It is just a simple xml -> struct conversion setup, but it saves time so it's included here.
The original code does not work out of the box and has a few faults, so it's included here.

## So you don't know how to run golang applications?

Follow this: [How to install golang](https://golang.org/doc/install)

Then: `go run main.go`

If you don't know how golang works, take a look at ["A tour of Go"](https://tour.golang.org/welcome/1). 
Finishing that should give a good understanding of how it works.

### Limitations

This project was made as a part of a minor assignment in a university class. It is therefore **not** stable, bug-free, reliable, or even good.
I'd never recommend using this project in anything other than small assignments. You might even have to patch it to make it work in some scenarios.

Some specifics:

[x] Doesn't support policy sets (might be simple to add, but unescessary for the assignment :shrug:)

[x] Probably does not separate the PDP, PEP, PIP, etc. sharply enough

[x] Only two matching functions have been implemented (out of, like, 50)

[x] Errors are not very helpful

[x] OnlyOneApplicable policy combination algorithm is not implemented

[x] And a bunch of other stuff I don't even remember or know!

## Documentation

OMG! Is that documentation? In *my* xacml implementation?!

---

Anyway. The documentation is mostly just examples of how to do stuff. I assume that's what most people want anyway.

It won't explain how xacml works, but it'll tell you how to implement some of the things supported.

### Policy Decision Point (PDP)

The PDP is an object that holds policies *and* evaluates requests (but does not manage obligations). It responds with a decision after evaluating a request.

The PDP is running as a separate thread in this implementation and waits for requests to be sent to it.

That means that you use two channels that are returned from the `PDP.Start()` function. One is used to send requests and the other receives decisions.

Using the PDP looks like this:

```go
import "xacmlpdp"

...

// Create object
pdp := xacmlpdp.NewPDP()

// Load policies into the object
pdp.LoadPolicies("./policies/")

// Start the PDP
requestChan, decisionChan := pdp.Start()

// See "Requests" below
req := xacmlcontext.NewRequest("./request.xml")

// Send the request to the PDP
requestChan <- req

// Wait for the response
// the program will wait here until it gets a response
decision <-decisionChan
```

A potential problem with multithreading use cases is that the decisions might not come back in order.

### Need more features?

Can't parse a request because the function doesn't exist?

Need Policy Set support?

Well do it yourself!

Functions can easily be implemented by adding them to the switch statement in the vendor/xacmlpdp/pdp `handleFunctions` function.
Just make sure to implement it.

The same can be done for many other things. Technically, the basic code is there. You just need to add some of the quirks from the specification.

### Objects

By "objects" we mean the elements in the xml.

You might need to mess with some of the objects yourself to do what you want to do.

I recommend looking at the [specification](https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf) for what each object does. Everything under section 5.x is under the `policy` package
and everything under section 6 is under the `xacmlcontext` package. Inspect the structs to see how they are organized 
(they should be similar to the spec)

### Consts

Not every const specified in the specification is present, but the main once are here, like the possible decisions (Permit, Deny, etc.), some functions, and many more. Have a look at vendor/pdp/consts.go for all of them.

### Requests

Requests are a type of "context" in the xacml world. We therefore use the `xacmlcontext` library to create requests:

```go

import "xacmlcontext"

...

req := xacmlcontext.NewRequest("./request.xml")

```

Requests are read as xml files from the file system in this library.

### Policy Object

The policy object *holds the contents of a single policy document*. You can create this object by:

```go

import (
    p "policy"
)

...

path = "./policy1"

p.NewPolicy(path)

```

This will automatically read the file from disk and return the object as a pointer.

