package main

import (
	"fmt"
	"log"
	"xacmlcontext"
	"xacmlpdp"
)

var policyPath string = "./policy/"
var requestPath string = "./request/request2.xml"

func main() {
	pdp := xacmlpdp.NewPDP(xacmlpdp.PolicyPermitOverrides)
	pdp.LoadPolicies(policyPath)

	requestChan, decisionChan := pdp.Start()

	req, err := xacmlcontext.NewRequest(requestPath)
	if err != nil {
		log.Fatalf("Could not read request at path %s with error %s", req, err.Error())
	}

	requestChan <- *req

	d := <-decisionChan
	fmt.Printf("%v\n", d.Results)
}
