package policy

import (
	"encoding/xml"
)

type AttributeSelector struct {
	XMLName xml.Name `xml:"AttributeSelector"`
}
