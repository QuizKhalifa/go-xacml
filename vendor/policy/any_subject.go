package policy

import (
	"encoding/xml"
)

type AnySubject struct {
	XMLName xml.Name `xml:"AnySubject"`
}
