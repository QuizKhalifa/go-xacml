package policy

import (
	"encoding/xml"
)

type Obligation struct {
	XMLName              xml.Name `xml:"Obligation"`
	ObligationId         string   `xml:"ObligationId,attr"`
	FullfillOn           string   `xml:"FulfillOn,attr"`
	AttributeAssignments []AttributeAssignment
}
