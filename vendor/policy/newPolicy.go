package policy

import (
	"encoding/xml"
	"fmt"
	"os"
)

func NewPolicy(path string) (*Policy, error) {
	// Open Policy file
	xmlFile, err := os.Open(path)
	if err != nil {
		fmt.Println("Error opening file:", err)
		return nil, err
	}
	// Close file when func exits
	defer xmlFile.Close()

	// Get some info about the file
	s, err := xmlFile.Stat()
	if err != nil {
		fmt.Println("Error stating file:", err)
		return nil, err
	}

	// Create a buffer to read into
	size := s.Size()
	b := make([]byte, size)
	xmlFile.Read(b)

	// Convert the xml to a Policy object
	var p Policy
	err = xml.Unmarshal([]byte(b), &p)
	if err != nil {
		fmt.Println("Error parsing policy document: ", err)
		return nil, err
	}

	return &p, nil
}
