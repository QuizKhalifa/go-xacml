package policy

import (
	"encoding/xml"
)

type AttributeDesignator struct {
	XMLName xml.Name `xml:"AttributeDesignator"`
}
