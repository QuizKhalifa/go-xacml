package policy

import (
	"encoding/xml"
)

type Attribute struct {
	XMLName xml.Name `xml:"Attribute"`
}
