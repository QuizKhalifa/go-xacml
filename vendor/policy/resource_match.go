package policy

import (
	"encoding/xml"
)

type ResourceMatch struct {
	XMLName                     xml.Name                    `xml:"ResourceMatch"`
	MatchId                     string                      `xml:"MatchId,attr"`
	AttributeValue              AttributeValue              `xml:"AttributeValue"`
	ResourceAttributeDesignator ResourceAttributeDesignator `xml:"ResourceAttributeDesignator"`
}
