package xacmlpdp

/*
Rule/policy combining algorithms
*/
const (
	RulePermitOverrides        = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:permit-overrides"
	RuleDenyOverrides          = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:deny-overrides"
	RuleOrderedDenyOverrides   = "urn:oasis:names:tc:xacml:1.1:rule-combining-algorithm:ordered-deny-overrides"
	RuleOrderedPermitOverrides = "urn:oasis:names:tc:xacml:1.1:rule-combining-algorithm:ordered-permit-overrides"
	RuleFirstApplicable        = "urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable"

	PolicyPermitOverrides        = "urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:permit-overrides"
	PolicyDenyOverrides          = "urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:deny-overrides"
	PolicyOrderedDenyOverrides   = "urn:oasis:names:tc:xacml:1.1:policy-combining-algorithm:ordered-deny-overrides"
	PolicyOrderedPermitOverrides = "urn:oasis:names:tc:xacml:1.1:policy-combining-algorithm:ordered-permit-overrides"
	PolicyFirstApplicable        = "urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:first-applicable"
	PolicyOnlyOneApplicable      = "urn:oasis:names:tc:xacml:1.0:policy-combining-algorithm:only-one-applicable"
)

const (
	DecisionPermit        = "Permit"
	DecisionDeny          = "Deny"
	DecisionNotApplicable = "NotApplicable"
	DecisionIndeterminate = "Indeterminate"
)

/*
Response Status
*/
const (
	// This identifier indicates success
	StatusOK = "urn:oasis:names:tc:xacml:1.0:status:ok"

	// This identifier indicates that all the attributes necessary to make a policy decision were not available (see Section 6.16).
	StatusMissingAttribute = "urn:oasis:names:tc:xacml:1.0:status:missing-attribute"

	//This identifier indicates that some attribute value contained a syntax error, such as a letter in a numeric field.
	StatusSyntaxError = "urn:oasis:names:tc:xacml:1.0:status:syntax-error"

	// This identifier indicates that an error occurred during policy evaluation. An example would be division by zero.
	StatusProcessingError = "urn:oasis:names:tc:xacml:1.0:status:processing-error"
)

const (
	StatusDescriptionOk               = "Success"
	StatusDescriptionMissingAttribute = "all the attributes necessary to make a policy decision were not available"
	StatusDescriptionSyntaxError      = "some attribute value contained a syntax error, such as a letter in a numeric field"
	StatusDescriptionProcessingError  = "Internal processing error"
)

/*
Functions
*/
const (
	FuncStringEqual                       = "urn:oasis:names:tc:xacml:1.0:function:string-equal"
	FuncBooleanEqual                      = "urn:oasis:names:tc:xacml:1.0:function:boolean-equal"
	FuncIntegerEqual                      = "urn:oasis:names:tc:xacml:1.0:function:integer-equal"
	FuncDoubleEqual                       = "urn:oasis:names:tc:xacml:1.0:function:double-equal"
	FuncDateEqual                         = "urn:oasis:names:tc:xacml:1.0:function:date-equal"
	FuncTimeEqual                         = "urn:oasis:names:tc:xacml:1.0:function:time-equal"
	FuncDateTimeEqual                     = "urn:oasis:names:tc:xacml:1.0:function:dateTime-equal"
	FuncDayTimeDurationEqual              = "urn:oasis:names:tc:xacml:1.0:function:dayTimeDuration-equal"
	FuncYearMonthDurationEqual            = "urn:oasis:names:tc:xacml:1.0:function:yearMonthDuration-equal"
	FuncAnyURIEqual                       = "urn:oasis:names:tc:xacml:1.0:function:anyURI-equal"
	FuncX500NameEqual                     = "urn:oasis:names:tc:xacml:1.0:function:x500Name-equal"
	FuncNameEqual                         = "urn:oasis:names:tc:xacml:1.0:function:rfc822Name-equal"
	FuncHexBinaryEqual                    = "urn:oasis:names:tc:xacml:1.0:function:hexBinary-equal"
	FuncBase64BinaryEqual                 = "urn:oasis:names:tc:xacml:1.0:function:base64Binary-equal"
	FuncIntegerAdd                        = "urn:oasis:names:tc:xacml:1.0:function:integer-add"
	FuncDoubleAdd                         = "urn:oasis:names:tc:xacml:1.0:function:double-add"
	FuncIntegerSubtract                   = "urn:oasis:names:tc:xacml:1.0:function:integer-subtract"
	FuncDoubleSubtract                    = "urn:oasis:names:tc:xacml:1.0:function:double-subtract"
	FuncIntegerMultiply                   = "urn:oasis:names:tc:xacml:1.0:function:integer-multiply"
	FuncDoubleMultiply                    = "urn:oasis:names:tc:xacml:1.0:function:double-multiply"
	FuncIntegerDivide                     = "urn:oasis:names:tc:xacml:1.0:function:integer-divide"
	FuncDoubleDivide                      = "urn:oasis:names:tc:xacml:1.0:function:double-divide"
	FuncIntegerMod                        = "urn:oasis:names:tc:xacml:1.0:function:integer-mod"
	FuncIntegerAbs                        = "urn:oasis:names:tc:xacml:1.0:function:integer-abs"
	FuncDoubleAbs                         = "urn:oasis:names:tc:xacml:1.0:function:double-abs"
	FuncRound                             = "urn:oasis:names:tc:xacml:1.0:function:round"
	FuncFloor                             = "urn:oasis:names:tc:xacml:1.0:function:floor"
	FuncStringNormalizeSpace              = "urn:oasis:names:tc:xacml:1.0:function:string-normalize-space"
	FuncStringNormalizeToLowerCase        = "urn:oasis:names:tc:xacml:1.0:function:string-normalize-to-lower-case"
	FuncDoubleToInteger                   = "urn:oasis:names:tc:xacml:1.0:function:double-to-integer"
	FuncIntegerToDouble                   = "urn:oasis:names:tc:xacml:1.0:function:integer-to-double"
	FuncOr                                = "urn:oasis:names:tc:xacml:1.0:function:or"
	FuncAnd                               = "urn:oasis:names:tc:xacml:1.0:function:and"
	FuncNOf                               = "urn:oasis:names:tc:xacml:1.0:function:n-of"
	FuncNot                               = "urn:oasis:names:tc:xacml:1.0:function:not"
	FuncIntegerGreaterThan                = "urn:oasis:names:tc:xacml:1.0:function:integer-greater-than"
	FuncIntegerGreaterThanOrEqual         = "urn:oasis:names:tc:xacml:1.0:function:integer-greater-than-or-equal"
	FuncIntegerLessThan                   = "urn:oasis:names:tc:xacml:1.0:function:integer-less-than"
	FuncIntegerLessThanOrEqual            = "urn:oasis:names:tc:xacml:1.0:function:integer-less-than-or-equal"
	FuncDoubleGreaterThan                 = "urn:oasis:names:tc:xacml:1.0:function:double-greater-than"
	FuncGreaterThanOrEqual                = "urn:oasis:names:tc:xacml:1.0:function:double-greater-than-or-equal"
	FuncDoubleLessThan                    = "urn:oasis:names:tc:xacml:1.0:function:double-less-than"
	FuncDoubleLessThanOrEqual             = "urn:oasis:names:tc:xacml:1.0:function:double-less-than-or-equal"
	FuncDateTimeAddDayTimeDuration        = "urn:oasis:names:tc:xacml:1.0:function:dateTime-add-dayTimeDuration"
	FuncDateTimeAddYearMonthDuration      = "urn:oasis:names:tc:xacml:1.0:function:dateTime-add-yearMonthDuration"
	FuncDateTimeSubtractDayTimeDuration   = "urn:oasis:names:tc:xacml:1.0:function:dateTime-subtract-dayTimeDuration"
	FuncDateTimeSubtractYearMonthDuration = "urn:oasis:names:tc:xacml:1.0:function:dateTime-subtractyearMonthDuration"
	FuncDateAddYearMonthDuration          = "urn:oasis:names:tc:xacml:1.0:function:date-add-yearMonthDuration"
	FuncDateSutractYearMonthDuration      = "urn:oasis:names:tc:xacml:1.0:function:date-subtract-yearMonthDuration"
	FuncStringGreatherThan                = "urn:oasis:names:tc:xacml:1.0:function:string-greater-than"
	FuncStringGreaterThanOrEqual          = "urn:oasis:names:tc:xacml:1.0:function:string-greater-than-or-equal"
	FuncStringLessThan                    = "urn:oasis:names:tc:xacml:1.0:function:string-less-than"
	FuncStringLessThanOrEqual             = "urn:oasis:names:tc:xacml:1.0:function:string-less-than-or-equal"
	FuncTimeGreaterThan                   = "urn:oasis:names:tc:xacml:1.0:function:time-greater-than"
	FuncTimeGreaterThanOrEqual            = "urn:oasis:names:tc:xacml:1.0:function:time-greater-than-or-equal"
	FuncTimeLessThan                      = "urn:oasis:names:tc:xacml:1.0:function:time-less-than"
	FuncTimeLessThanOrEqual               = "urn:oasis:names:tc:xacml:1.0:function:time-less-than-or-equal"
	FuncTimeInRange                       = "urn:oasis:names:tc:xacml:2.0:function:time-in-range"
	FuncDateTimeGreaterThan               = "urn:oasis:names:tc:xacml:1.0:function:dateTime-greater-than"
	FuncDateTimeGreaterThanOrEqual        = "urn:oasis:names:tc:xacml:1.0:function:dateTime-greater-than-or-equal"
	FuncDateTimeLessThan                  = "urn:oasis:names:tc:xacml:1.0:function:dateTime-less-than"
)
