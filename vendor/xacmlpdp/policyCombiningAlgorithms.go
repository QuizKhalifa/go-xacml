package xacmlpdp

import (
	"policy"
)

func denyOverridesPolicyCombiningAlgorithm(subjectIds, resources, actions []string, policies []*policy.Policy) ([]policy.Obligation, string, error) {
	var atLeastOnePermit bool = false
	var policiesThatEvaluateToPermit []*policy.Policy
	var err error
	var decision string

	for _, policy := range policies {
		decision, err = evaluatePolicy(subjectIds, resources, actions, policy)
		if decision == DecisionDeny {
			return policy.Obligations, DecisionDeny, err
		}
		if decision == DecisionPermit {
			atLeastOnePermit = true
			policiesThatEvaluateToPermit = append(policiesThatEvaluateToPermit, policy)
			continue
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			return policy.Obligations, DecisionDeny, err
		}
	}
	if atLeastOnePermit {
		// NOTE THAT DUPLICATES ARE NOT REMOVED!!
		allObligations := make([]policy.Obligation, 0, 10)
		for _, policy := range policiesThatEvaluateToPermit {
			allObligations = append(allObligations, policy.Obligations...)
		}
		return allObligations, DecisionPermit, err
	}
	return nil, DecisionNotApplicable, err
}

func permitOverridesPolicyCombiningAlgorithm(subjectIds, resources, actions []string, policies []*policy.Policy) ([]policy.Obligation, string, error) {
	var atLeastOneError bool = false
	var atLeastOneDeny bool = false
	var policiesThatEvaluateToDeny []*policy.Policy = make([]*policy.Policy, 0, 10)
	var err error
	var decision string

	for _, policy := range policies {
		decision, err = evaluatePolicy(subjectIds, resources, actions, policy)
		if decision == DecisionDeny {
			atLeastOneDeny = true
			policiesThatEvaluateToDeny = append(policiesThatEvaluateToDeny, policy)
			continue
		}
		if decision == DecisionPermit {
			return policy.Obligations, DecisionPermit, err
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			atLeastOneError = true
			continue
		}
	}
	if atLeastOneDeny {
		allObligations := make([]policy.Obligation, 0, 10)
		for _, policy := range policiesThatEvaluateToDeny {
			allObligations = append(allObligations, policy.Obligations...)
		}
		return allObligations, DecisionDeny, err
	}
	if atLeastOneError {
		return nil, DecisionIndeterminate, err
	}
	return nil, DecisionNotApplicable, err
}

// C.5 (p.136) in https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf
func firstApplicableEffectPolicyCombiningAlgorithm(subjectIds, resources, actions []string, policies []*policy.Policy) ([]policy.Obligation, string, error) {
	var err error
	var decision string
	for _, policy := range policies {
		decision, err = evaluatePolicy(subjectIds, resources, actions, policy)
		if decision == DecisionDeny {
			return policy.Obligations, DecisionDeny, err
		}
		if decision == DecisionPermit {
			return policy.Obligations, DecisionPermit, err
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			return nil, DecisionIndeterminate, err
		}
	}
	return nil, DecisionNotApplicable, err
}

// C.6 (p. 138) in https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf
// Mind that this is not actually fully implemented yet. It *will* fail
func onlyOneApplicablePolicyPolicyCombiningAlogrithm(subjectIds, resources, actions []string, policies []*policy.Policy) ([]policy.Obligation, string, error) {
	var atLeastOne bool = false
	var applicable string
	var selectedPolicy *policy.Policy

	for _, policy := range policies {
		applicable = isApplicable(policy)

		if applicable == DecisionIndeterminate {
			return nil, DecisionIndeterminate, nil
		}
		if applicable == "Applicable" {
			if atLeastOne {
				return nil, DecisionIndeterminate, nil
			} else {
				atLeastOne = true
				selectedPolicy = policy
			}
		}
		if applicable == DecisionNotApplicable {
			continue
		}
	}
	if atLeastOne {
		decision, err := evaluatePolicy(subjectIds, resources, actions, selectedPolicy)
		return selectedPolicy.Obligations, decision, err
	} else {
		return nil, DecisionNotApplicable, nil
	}
}

// isApplicable tells us if the policy applies to these targets
// TODO: Impl
func isApplicable(policy *policy.Policy) string {
	return DecisionPermit
}
