package xacmlpdp

import (
	"fmt"
	"io/ioutil"
	"log"
	"policy"
	"strings"
	"xacmlcontext"
)

type pdp struct {
	PolicySets               []*policy.PolicySet
	Policies                 []*policy.Policy
	PolicyCombiningAlgorithm string
}

// NewPDP - Returns an empty PDP
func NewPDP(policyCombiningAlgorithm string) pdp {
	return pdp{
		PolicyCombiningAlgorithm: policyCombiningAlgorithm,
	}
}

// Starts the pdp in a separate thread.
// @return xacmlcontext.Request: Send requests on this channel
// @return xacmlcontext.Decision: Receive decisions on this channel
func (p *pdp) Start() (chan xacmlcontext.Request, chan xacmlcontext.Response) {
	requestPipe := make(chan xacmlcontext.Request)
	decisionPipe := make(chan xacmlcontext.Response)
	go p.run(requestPipe, decisionPipe)

	return requestPipe, decisionPipe
}

// run - Thread that handles requests and makes decisions
func (p *pdp) run(requestPipe chan xacmlcontext.Request, decisionPipe chan xacmlcontext.Response) {
	// Wait for requests and make decisions
	for req := range requestPipe {

		// Respond
		// TODO: Might be out of order if multiple instances send requests
		decision := p.evaluateRequest(req)
		decisionPipe <- decision
	}
}

// evaluateRequest - receives a request and evaluates it
func (p *pdp) evaluateRequest(req xacmlcontext.Request) xacmlcontext.Response {

	// The request has a set of subjects that wants to access some resource
	subjectIds := make([]string, 0, 10)
	for _, subject := range req.Subjects {
		for _, attribute := range subject.Attributes {
			if attribute.AttributeId == "urn:oasis:names:tc:xacml:1.0:subject:subject-id" {
				// This assumes that attributes with "subject-id" have exactly one attributeValue
				subjectIds = append(subjectIds, attribute.AttributeValues[0].Value)
				break
			}
		}
	}

	// What resources?
	resources := make([]string, 0, 10)
	for _, subject := range req.Resources {
		for _, attribute := range subject.Attributes {
			if attribute.AttributeId == "urn:oasis:names:tc:xacml:1.0:resource:resource-id" {
				// This assumes that attributes with "subject-id" have exactly one attributeValue
				resources = append(resources, attribute.AttributeValues[0].Value)
				break
			}
		}
	}

	// What action do they want to take?
	actions := make([]string, 0, 10)
	for _, attribute := range req.Action.Attributes {
		for _, value := range attribute.AttributeValues {
			actions = append(actions, value.Value)
		}
	}

	obligations, decision, err := p.evaluateAllPolicies(subjectIds, resources, actions)

	return xacmlcontext.Response{
		Xmlns:          "urn:oasis:names:tc:xacml:1.0:context",
		Xsi:            "http://www.w3.org/2001/XMLSchema-instance",
		SchemaLocation: "urn:oasis:names:tc:xacml:1.0:context\ncs-xacml-schema-context-01.xsd", // TODO: Non-static. Depends on value
		Results:        createResult(decision, obligations, err),
	}
}

func createResult(decision string, obligations []policy.Obligation, err error) []xacmlcontext.Result {

	// Create the status information
	stat := xacmlcontext.Status{}

	if decision == DecisionPermit || decision == DecisionDeny {
		stat = xacmlcontext.Status{
			StatusCode: xacmlcontext.StatusCode{
				Value: StatusOK,
			},
			StatusMessage: xacmlcontext.StatusMessage{
				Value: StatusDescriptionOk,
			},
		}
	} else {
		// If the status is "processing-error" we shouldn't show what the problem was
		// because the error is internal
		if strings.HasPrefix(err.Error(), "processing-error") {
			stat = xacmlcontext.Status{
				StatusCode: xacmlcontext.StatusCode{
					Value: StatusProcessingError,
				},
				StatusMessage: xacmlcontext.StatusMessage{
					Value: StatusDescriptionProcessingError,
				},
			}
		} else {
			// Now it is either "syntax-error" or "missing-attribute"
			// These needs actuall testing, so it won't be implemented here :)
			stat = xacmlcontext.Status{
				StatusCode: xacmlcontext.StatusCode{
					Value: StatusOK,
				},
				StatusMessage: xacmlcontext.StatusMessage{
					Value: "Not actually OK :/",
				},
				StatusDetail: xacmlcontext.StatusDetail{
					MissingAttributeDetails: []xacmlcontext.MissingAttributeDetail{
						{
							AttributeValue: xacmlcontext.AttributeValue{
								Value: err.Error(),
							},
						},
					},
				},
			}
		}
	}

	res := make([]xacmlcontext.Result, 0, 1)
	res = append(res, xacmlcontext.Result{
		Decision:    xacmlcontext.Decision{Decision: decision},
		Status:      stat,
		Obligations: obligations,
	})

	return res
}

func (p *pdp) evaluateAllPolicies(subjectIds, resources, actions []string) ([]policy.Obligation, string, error) {
	switch p.PolicyCombiningAlgorithm {
	case PolicyPermitOverrides, PolicyOrderedPermitOverrides:
		return permitOverridesPolicyCombiningAlgorithm(subjectIds, resources, actions, p.Policies)
	case PolicyDenyOverrides, PolicyOrderedDenyOverrides:
		return denyOverridesPolicyCombiningAlgorithm(subjectIds, resources, actions, p.Policies)
	case PolicyFirstApplicable:
		return firstApplicableEffectPolicyCombiningAlgorithm(subjectIds, resources, actions, p.Policies)
	case PolicyOnlyOneApplicable:
		// Not fully implemented
		return onlyOneApplicablePolicyPolicyCombiningAlogrithm(subjectIds, resources, actions, p.Policies)
	}
	return nil, "", fmt.Errorf("processing-error: Policy has invalid combining algorithm")
}

func evaluatePolicy(subjectIds, resources, actions []string, pol *policy.Policy) (string, error) {
	switch pol.RuleCombiningAlgId {
	case RulePermitOverrides, RuleOrderedPermitOverrides:
		return permitOverridesRuleCombiningAlgorithm(subjectIds, resources, actions, pol.Rules)
	case RuleDenyOverrides, RuleOrderedDenyOverrides:
		return denyOverridesRuleCombiningAlgorithm(subjectIds, resources, actions, pol.Rules)
	case RuleFirstApplicable:
		return firstApplicableEffectRuleCombiningAlgorithm(subjectIds, resources, actions, pol.Rules)
	}
	return "", fmt.Errorf("processing-error: Invalid rule combining algorithm")
}

// Check the policy
// This implementation is extremely naive and slow
// But since this is not for a real system, it is fine
func evaluateRule(subjectIds, resources, actions []string, rule policy.Rule) (string, error) {
	// Does the rule contain the desired actions
	for _, action := range actions {
		contains, err := ruleContainsAction(action, &rule)
		if err != nil {
			return DecisionIndeterminate, err
		}
		if !contains {
			return DecisionDeny, fmt.Errorf("message: action on resource is not allowed")
		}
	}

	// Does it contain the resource?
	for _, resource := range resources {
		contains, err := ruleContainsResource(resource, &rule)
		if err != nil {
			return DecisionIndeterminate, err
		}
		if !contains {
			return DecisionNotApplicable, fmt.Errorf("message: no rule for subject-resource relationship")
		}
	}

	// Does the rule contain the subject?
	for _, id := range subjectIds {
		contains, err := ruleContainsSubject(id, &rule)
		if err != nil {
			return DecisionIndeterminate, err
		}
		if !contains {
			return DecisionNotApplicable, fmt.Errorf("message: no rule for this subject")
		}
	}

	// Does the rule match
	return rule.Effect, nil
}

func ruleContainsAction(desiredAction string, rule *policy.Rule) (bool, error) {
	// If the rule has no action, then allow all actions
	if len(rule.Target.Actions.Actions) == 0 {
		return true, nil
	}

	// Otherwise, check if it contains the action
	for _, action := range rule.Target.Actions.Actions {
		for _, actionMatcher := range action.ActionMatch {
			matchMethod := actionMatcher.MatchId
			outcome, err := handleFunctions(matchMethod, actionMatcher.AttributeValue.Value, desiredAction)
			if err != nil || outcome == nil {
				return false, err
			}
			if outcome.(bool) {
				return true, nil
			}
		}
	}
	return false, nil
}

func ruleContainsResource(resourceId string, rule *policy.Rule) (bool, error) {
	// If the rule has no resource, it applies to all resources
	if len(rule.Target.Resources.Resources) == 0 {
		return true, nil
	}
	for _, resource := range rule.Target.Resources.Resources {
		for _, resourceMatcher := range resource.ResourceMatch {
			matchMethod := resourceMatcher.MatchId
			outcome, err := handleFunctions(matchMethod, resourceMatcher.AttributeValue.Value, resourceId)
			if err != nil || outcome == nil {
				return false, err
			}
			if outcome.(bool) {
				return true, nil
			}
		}
	}
	return false, nil
}

func ruleContainsSubject(subjectId string, rule *policy.Rule) (bool, error) {
	// If the rule has no subject, it applies to all subjects
	if len(rule.Target.Subjects.Subjects) == 0 {
		return true, nil
	}
	for _, target := range rule.Target.Subjects.Subjects {
		for _, subjectMatcher := range target.SubjectMatch {
			matchMethod := subjectMatcher.MatchId
			outcome, err := handleFunctions(matchMethod, subjectMatcher.AttributeValue.Value, subjectId)
			if err != nil || outcome == nil {
				return false, err
			}

			if outcome.(bool) {
				return true, nil
			}
		}
	}
	return false, nil
}

// handleFunctions - takes the function name and parameters as input and returns the result
func handleFunctions(function string, params ...interface{}) (interface{}, error) {
	switch function {
	case FuncStringEqual:
		return stringEqual(function, params)

	case FuncAnyURIEqual:
		return anyURIEqual(function, params)

		// No match. Pack up and go home cuz that shit ain't implemented
	default:
		return nil, fmt.Errorf("No function matching \"%s\"", function)
	}
}

// Loads all policies
// Overrides current list
// @param pathToPolicies: Path to a folder with (only) policies
func (p *pdp) LoadPolicies(pathToPolicies string) {
	// Load policies
	p.Policies = make([]*policy.Policy, 0, 10)
	allPolicies := loadPathToPolicies(pathToPolicies)
	for _, path := range allPolicies {
		pol, err := policy.NewPolicy(path)
		if err != nil {
			fmt.Printf("Could not load policy from file %s with error %s\n", path, err.Error())
		}

		p.Policies = append(p.Policies, pol)
	}
}

// AddPolicy - Different from LoadPolicies that it loads a single policy file
// and doesn't override the old list
func (p *pdp) AddPolicy(path string) {
	pol, err := policy.NewPolicy(path)
	if err != nil {
		fmt.Printf("Could not load policy from file %s with error %s\n", path, err.Error())
	}
	p.Policies = append(p.Policies, pol)
}

// loadPathToPolicies - Goes through the file structure and gets the path to all policies at once
func loadPathToPolicies(pathToPolicies string) []string {
	fileList, err := ioutil.ReadDir(pathToPolicies)

	pathList := make([]string, 0, 10)
	if err != nil {
		log.Fatal("Path to policy-documents does not exist!")
	}
	for _, file := range fileList {
		if file.IsDir() {
			tmp := loadPathToPolicies(pathToPolicies + file.Name() + "/")
			pathList = append(pathList, tmp...)
		} else {
			pathList = append(pathList, pathToPolicies+file.Name())
		}
	}

	return pathList
}
