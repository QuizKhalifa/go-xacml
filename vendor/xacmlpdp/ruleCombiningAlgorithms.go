package xacmlpdp

import (
	"fmt"
	"policy"
)

// C.1 (p. 132) in https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf
func denyOverridesRuleCombiningAlgorithm(subjectIds, resources, actions []string, rules []policy.Rule) (string, error) {
	var atLeastOneError bool = false
	var potentialDeny bool = false
	var atLeastOnePermit bool = false
	var err error
	var decision string

	for _, rule := range rules {
		decision, err = evaluateRule(subjectIds, resources, actions, rule)
		if decision == DecisionDeny {
			return DecisionDeny, err
		}
		if decision == DecisionPermit {
			atLeastOnePermit = true
			continue
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			atLeastOneError = true
			if rule.Effect == DecisionDeny {
				potentialDeny = true
			}
			continue
		}
	}
	if potentialDeny {
		return DecisionIndeterminate, fmt.Errorf("message: a clear choice could not be made")
	}
	if atLeastOnePermit {
		return DecisionPermit, nil
	}
	if atLeastOneError {
		return DecisionIndeterminate, err
	}
	return DecisionNotApplicable, fmt.Errorf("message: rule does not match")
}

// C.3 (134) in https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf
func permitOverridesRuleCombiningAlgorithm(subjectIds, resources, actions []string, rules []policy.Rule) (string, error) {
	var atLeastOneError bool = false
	var potentialPermit bool = false
	var atLeastOneDeny bool = false
	var err error
	var denyError error
	var atLeastError error
	var decision string

	for _, rule := range rules {
		decision, err = evaluateRule(subjectIds, resources, actions, rule)
		if decision == DecisionDeny {
			atLeastOneDeny = true
			denyError = err
			continue
		}
		if decision == DecisionPermit {
			return DecisionPermit, nil
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			atLeastOneError = true
			atLeastError = err
			if rule.Effect == DecisionPermit {
				potentialPermit = true
			}
			continue
		}
	}
	if potentialPermit {
		return DecisionIndeterminate, fmt.Errorf("message: a clear choice could not be made")
	}
	if atLeastOneDeny {
		return DecisionDeny, denyError
	}
	if atLeastOneError {
		return DecisionIndeterminate, atLeastError
	}
	return DecisionNotApplicable, fmt.Errorf("message: rule does not match")
}

// C.5 (p.136) in https://docs.oasis-open.org/xacml/2.0/access_control-xacml-2.0-core-spec-os.pdf
func firstApplicableEffectRuleCombiningAlgorithm(subjectIds, resources, actions []string, rules []policy.Rule) (string, error) {
	var err error
	var decision string
	for _, rule := range rules {
		decision, err = evaluateRule(subjectIds, resources, actions, rule)
		if decision == DecisionDeny {
			return DecisionDeny, err
		}
		if decision == DecisionPermit {
			return DecisionPermit, err
		}
		if decision == DecisionNotApplicable {
			continue
		}
		if decision == DecisionIndeterminate {
			return DecisionIndeterminate, err
		}
	}
	return DecisionNotApplicable, err
}
