package xacmlpdp

import (
	"fmt"
	"reflect"
	"regexp"
)

// Implementation of "urn:oasis:names:tc:xacml:1.0:function:string-equal"
func stringEqual(function string, params []interface{}) (interface{}, error) {
	return twoStringFunction(function, params, func(a, b string) (interface{}, error) {
		return params[0] == params[1], nil
	})
}

// Implementation of "urn:oasis:names:tc:xacml:1.0:function:anyURI-equal"
func anyURIEqual(function string, params []interface{}) (interface{}, error) {

	re := regexp.MustCompile(`(?i)([a-z]+\:\/+)([^\/\s]+)([a-z0-9\-@\^=%&;\/~\+]*)[\?]?([^ \#\r\n]*)#?([^ \#\r\n]*)`)
	return twoStringFunction(function, params, func(a, b string) (interface{}, error) {
		if re.MatchString(a) && re.MatchString(b) && a == b {
			return true, nil
		}
		return false, nil
	})
}

func twoStringFunction(function string, params []interface{}, f func(string, string) (interface{}, error)) (interface{}, error) {
	if len(params) == 2 {
		if _, ok := params[0].(string); ok {
			if _, ok := params[1].(string); ok {
				return f(params[0].(string), params[1].(string))
			}
		}
		return nil, fmt.Errorf("function \"%s\"requires two strings but got %s and %s", function, reflect.TypeOf(params[0]), reflect.TypeOf(params[1]))
	}
	return nil, fmt.Errorf("Function requires 2 parameters. Received only %d", len(params))
}
