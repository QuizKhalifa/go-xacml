package xacmlcontext

import (
	"encoding/xml"
)

/*
The <StatusCode> element contains a major status code value and an optional sequence of
minor status codes
*/
type StatusCode struct {
	XMLName          xml.Name          `xml:"StatusCode"`
	Value            string            `xml:"Value,attr"`
	MinorStatusCodes []MinorStatusCode `xml:"StatusCode"`
}

type MinorStatusCode struct {
	XMLName xml.Name `xml:"StatusCode"`
	Value   string   `xml:"Value,attr"`
}
