package xacmlcontext

import (
	"encoding/xml"
)

/*
The <StatusDetail> element qualifies the <Status> element with additional information
*/
type StatusDetail struct {
	XMLName                 xml.Name                 `xml:"StatusDetail"`
	MissingAttributeDetails []MissingAttributeDetail `xml:"MissingAttributeDetail"`
}
