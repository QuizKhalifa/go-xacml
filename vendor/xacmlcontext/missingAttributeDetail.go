package xacmlcontext

import (
	"encoding/xml"
)

/*
The <MissingAttributeDetail> element conveys information about attributes required for
policy evaluation that were missing from the request context
*/
type MissingAttributeDetail struct {
	XMLName        xml.Name       `xml:"MissingAttributeDetail"`
	AttributeValue AttributeValue `xml:"AttributeValue"`
	AttributeId    string         `xml:"AttributeId,attr"`
	DataType       string         `xml:"DataType,attr"`
	Issuer         string         `xml:"Issuer,attr"`
}
