package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Environment> element contains a set of attributes of the environment.
*/
type Environment struct {
	XMLName    xml.Name    `xml:"Environment"`
	Attributes []Attribute `xml:"Attribute"`
}
