package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Resource> element specifies information about the resource to which access is requested,
by listing a sequence of <Attribute> elements associated with the resource. It MAY include the
resource content
*/
type Resource struct {
	XMLName         xml.Name        `xml:"Resource"`
	ResourceContent ResourceContent `xml:"ResourceContent"`
	Attributes      []Attribute     `xml:"Attribute"`
}
