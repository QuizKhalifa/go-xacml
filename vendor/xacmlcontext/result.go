package xacmlcontext

import (
	"encoding/xml"
	"policy"
)

/*
The <Result> element represents an authorization decision result for the resource specified by
the ResourceId attribute. It MAY include a set of obligations that MUST be fulfilled by the PEP.
If the PEP does not understand or cannot fulfill an obligation, then it MUST act as if the PDP had
denied access to the requested resource
*/
type Result struct {
	XMLName     xml.Name            `xml:"Result"`
	ResourceId  string              `xml:"ResourceId,attr"`
	Decision    Decision            `xml:"Decision"`
	Status      Status              `xml:"Status"`
	Obligations []policy.Obligation `xml:"Obligations>Obligation"`
}
