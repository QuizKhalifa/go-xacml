package xacmlcontext

import (
	"encoding/xml"
)

/*
The <ResourceContent> element is a notional placeholder for the content of the resource. If an
XACML policy references the contents of the resource by means of an <AttributeSelector>
element, then the <ResourceContent> element MUST be included in the
RequestContextPath string.

TODO: IMPL
*/
type ResourceContent struct {
	XMLName xml.Name `xml:"ResourceContent"`
}
