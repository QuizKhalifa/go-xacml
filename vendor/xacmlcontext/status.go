package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Status> element represents the status of the authorization decision result
*/
type Status struct {
	XMLName       xml.Name      `xml:"Status"`
	StatusCode    StatusCode    `xml:"StatusCode"`
	StatusMessage StatusMessage `xml:"StatusMessage"`
	StatusDetail  StatusDetail  `xml:"StatusDetail"`
}
