package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Request> element is a top-level element in the XACML context schema. The <Request>
element is an abstraction layer used by the policy language. For simplicity of expression, this
document describes policy evaluation in terms of operations on the context. However a
conforming PDP is not required to actually instantiate the context in the form of an XML document.
But, any system conforming to the XACML specification MUST produce exactly the same
authorization decisions as if all the inputs had been transformed into the form of an <xacml2865 context:Request> element.
*/
type Request struct {
	XMLName     xml.Name    `xml:"Request"`
	Subjects    []Subject   `xml:"Subject"`
	Resources   []Resource  `xml:"Resource"`
	Action      Action      `xml:"Action"`
	Environment Environment `xml:"Environment"`
}
