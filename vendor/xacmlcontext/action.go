package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Action> element specifies the requested action on the resource, by listing a set of
<Attribute> elements associated with the action.
*/
type Action struct {
	XMLName    xml.Name    `xml:"Action"`
	Attributes []Attribute `xml:"Attribute"`
}
