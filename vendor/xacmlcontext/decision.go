package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Decision> element contains the result of policy evaluation.
*/
type Decision struct {
	XMLName  xml.Name `xml:"Decision"`
	Decision string   `xml:",chardata"`
}
