package xacmlcontext

import (
	"encoding/xml"
)

type AttributeAssignment struct {
	XMLName     xml.Name `xml:"AttributeAssignment"`
	AttributeId string   `xml:"AttributeId,attr"`
	Value       string   `xml:",chardata"`
	Category    string
	Issuer      string
}
