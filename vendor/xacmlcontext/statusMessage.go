package xacmlcontext

import (
	"encoding/xml"
)

/*
The <StatusMessage> element is a free-form description of the status code.
*/
type StatusMessage struct {
	XMLName xml.Name `xml:"StatusMessage"`
	Value   string   `xml:",chardata"`
}
