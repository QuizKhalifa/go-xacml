package xacmlcontext

import (
	"encoding/xml"
)

/*
The <xacml-context:AttributeValue> element contains the value of an attribute.
*/
type AttributeValue struct {
	XMLName xml.Name `xml:"AttributeValue"`
	Value   string   `xml:",chardata"`
}
