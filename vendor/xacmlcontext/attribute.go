package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Attribute> element is the central abstraction of the request context. It contains attribute
meta-data and one or more attribute values. The attribute meta-data comprises the attribute
identifier and the attribute issuer. <AttributeDesignator> and <AttributeSelector>
elements in the policy MAY refer to attributes by means of this meta-data.
*/
type Attribute struct {
	XMLName         xml.Name         `xml:"Attribute"`
	AttributeId     string           `xml:"AttributeId,attr"`
	DataType        string           `xml:"DataType,attr"`
	Issuer          string           `xml:"Issuer,attr"`
	AttributeValues []AttributeValue `xml:"AttributeValue"`
}
