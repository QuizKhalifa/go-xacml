package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Subject> element specifies a subject by listing a sequence of <Attribute> elements
associated with the subject.
*/
type Subject struct {
	XMLName         xml.Name    `xml:"Subject"`
	SubjectCategory string      `xml:"SubjectCategory,attr"`
	Attributes      []Attribute `xml:"Attribute"`
}
