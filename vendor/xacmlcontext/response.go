package xacmlcontext

import (
	"encoding/xml"
)

/*
The <Response> element is a top-level element in the XACML context schema. The
<Response> element is an abstraction layer used by the policy language. Any proprietary
system using the XACML specification MUST transform an XACML context <Response> element
into the form of its authorization decision.
*/
type Response struct {
	XMLName        xml.Name `xml:"Response"`
	Xmlns          string   `xml:"xmlns,attr"`
	Xsi            string   `xml:"xmlns:xsi,attr"`
	SchemaLocation string   `xml:"schemaLocation,attr"`
	Results        []Result `xml:"Result"`
}
